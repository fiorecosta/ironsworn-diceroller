# IronSworn-DiceRoller

The Ironsworn-DiceRoller app is a small application allowing for Dice Rolling and interpretation of 
the Ironsworn system including the core game (Ironsworn), Delve and Starforged. 

More Information can be obtained via : https://www.ironswornrpg.com

## Information
The dice roller uses a random number generator to simulate the rolling of three dice (two challenge 
dice and one action dice) as per the Ironsworn rule-set. The dice is then interpreted for the user to 
indicate if they have succeded or failed their roll and what the subsequent outcome / magnitude should be.

The final version will be provided in two forms, a Pyhton Script for use in a terminal / python ide and a graphical user interface for those 
wishing to use the app as a standard desktop application. 

### Version 
1.0


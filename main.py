# IronSworn Dice Roller
import random
import tkinter as tk


# Dice roller function

def roller():
    # Get values from GUI
    stat = statTxt.get()
    m = mTxt.get()

    # Dice Roller for 2c10 and 1a
    c1 = random.randint(1, 10)
    c2 = random.randint(1, 10)
    a = random.randint(1, 6) + int(m) + int(stat)

    def result():
        if c1 == c2:
            if a > c1 and a > c2:
                return "MATCH - Strong Hit"
            elif a == c1 and a == c2:
                return "MATCH - Miss"
            elif a >= c1 or a > c2:
                return "MATCH - Weak Hit"
            elif a < c1 and a < c2:
                return "MATCH - Miss"
            else:
                "UNDEFINED"
        else:
            if a > c1 and a > c2:
                return "Strong Hit"
            elif a <= c1 and a > c2:
                return  "Weak Hit"
            elif a == c1 and a == c2:
                return "      Miss     "
            elif a > c1 or a > c2:                return "Weak Hit"
            elif a < c1 and a < c2:
                return "      Miss     "
            else:
                "UNDEFINED"

    result = str(result())

    diced1result = tk.Label(window, text=c1, fg="black")
    diced1result.grid(column=1, row=4)

    diced2result = tk.Label(window, text=c2, fg="black")
    diced2result.grid(column=2, row=4)

    a = tk.Label(window, text=a, fg="black")
    a.grid(column=1, row=5)

    final = tk.Label(window, text=result, fg="red").place(x=115, y=150)

    return c1, c2, a


# Window Geometry setup
window = tk.Tk()
window.geometry('350x230')
window.title("IronSworn - DiceRoller V1.0")
lbl = tk.Label(window, text="V1.0 - Alpha")
lbl.grid(column=0, row=0)

# Input
statLbl = tk.Label(window, text="Stat : ")
statLbl.grid(column=0, row=1)
statTxt = tk.Entry(window, width=2)
statTxt.insert(0, "0")
statTxt.grid(column=1, row=1)

mLbl = tk.Label(window, text="Adds : ")
mLbl.grid(column=0, row=2)
mTxt = tk.Entry(window, width=2)
mTxt.insert(0, "0")
mTxt.grid(column=1, row=2)

# Roll Button Details
roll_button = tk.Button(window, text="ROLL", fg="green", bg="red", command=roller).place(x=190, y=50)

# Dice Output
diceD1Lbl = tk.Label(window, text="Challenge Dice ")
diceD1Lbl.grid(column=0, row=4)

diceD1Lbl = tk.Label(window, text="Action + Mods")
diceD1Lbl.grid(column=0, row=5)

# Main Window Spawner
window.mainloop()

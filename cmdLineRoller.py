# IronSwron Dice Roller
import random
import tkinter as tk

# Dice roller function
def roller():
    print("IronSworn Dice Roller")

    # Request (and validate) value of modifiers
    # SET TO OFF DUE TO GUI EXPERIMENT!!!
    stat = statValue()
    m = actionModifierValue()

    # Dice Roller for 2d10 and 1d6
    d10_1 = random.randint(1, 10)
    d10_2 = random.randint(1, 10)
    d6 = random.randint(1, 6)

    diceStack = []
    diceStack.append(d10_1)
    diceStack.append(d10_2)
    diceStack.append(d6)
    diceStack[2] = diceStack[2] + int(m) + int(stat)
    return diceStack

# set action modifier value and
def actionModifierValue():
    while True:
        m = input("Enter ADDS modifier value = ") or 0
        try:
            if (int(m) >= -5 and int(m) < 5):
                return m
            else:
                print("Modifier not accepted. Try again >> ")
        except:
            print("Value not recognised as a valid value. Try again >> ")
# Set value and validate input
def statValue():
    while True:
        s = input("Enter STAT Modifier Value = ") or 0
        try:
            if (int(s) >= -5 and int(s) < 5):
                intChecker = False
                return s
            else:
                print("Modifier not accepted. Try again >> ")
        except:
            print("Value not recognised as a valid value. Try again >> ")

def diceOutput():
    print("Challenge dice : " + str(diceStack[0]) + " & " + str(diceStack[1]))
    print("Action die :" + str(diceStack[2]))
    print("******************")
    if (diceStack[0] and diceStack[1]) < diceStack[2]:
        print("STRONG hit")
    elif (diceStack[0] or diceStack[1]) < diceStack[2]:
        print("WEAK hit")
    elif (diceStack[0] and diceStack[1]) > diceStack[2]:
        print("MISS")
    else:
        pass

    if (diceStack[0] == diceStack[1]):
        print(" ** Challenge dice MATCHES ** ")
        print(" Increase narative strength (page 9 of Ironswron book)")
    else:
        pass

def clickRoll():
    print("Hello World!")

# Run Main functions
diceStack = roller()

#Output Debuger
diceOutput()